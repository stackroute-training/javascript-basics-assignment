/* Write a program to build a `Pyramid of stars` of given height */

const pyramid = (n) => {
     if(Number.isInteger(n)){
          var a = ""
          for (i = 1; i <= n; i++) {
               let ans = "";
               for (j = 0; j <= (n-i); j++) {
                    ans += " "
               }

               for (k = 1; k <= i; k++) {
                    if(k == 1){
                         ans += "*"
                    }
                    else{                       
                         ans += " *"
                    }                    
               }
               for(l=7;l<9;l++){
                    ans += " " 
               }
               a += ans +"\n"
          }
          return a
     }
     else{
         return ''
     }

}



/* For example,
INPUT - buildPyramid(6)
OUTPUT -
     *
    * *
   * * *
  * * * *
 * * * * *
* * * * * *

*/

module.exports = pyramid;
