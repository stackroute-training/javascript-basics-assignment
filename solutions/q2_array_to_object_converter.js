/* Write a Program to convert an array of objects to an object
	based on a given key */


const convert = (arg1,arg2) => {
	// Write your code here
	let c = {};
	if(!(Array.isArray(arg1))){
		return null;
	}
	else{
		for(x of arg1){
			c[x[arg2]] = x;
		}
		return c;
	}	
};


/* For example,
INPUT - convert([{id: 1, value: 'abc'}, {id: 2, value: 'xyz'}], 'id')
OUTPUT - {
			'1': {id: 1, value: 'abc'},
			'2': {id: 2, value: 'xyz'}
		 }


*/

module.exports = convert;
