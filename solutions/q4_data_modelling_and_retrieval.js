// Create a list of fruits with their properties (name, color, pricePerKg)
// and convert it into a format so that for a given fruit name
// retrieval of its color and pricePerKg value is fast
const readline = require("readline"); // Importing readline from nodejs

// Creting the stream line interface 
const interFace = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

// taking input from the user
interFace.question("Please enter the fruit Name ", function (answer) {
    retrieval(answer);
    interFace.close();
});

// Storing fruits data in an array of objects
var fruits = [
    { "Name": "Mango", "color": "Yellow", "pricePerKg": 50 },
    { "Name": "Grapes", "color": "Green", "pricePerKg": 20 },
    { "Name": "BlueBerries", "color": "Blue", "pricePerKg": 80 },
    { "Name": "StrawBerries", "color": "Red", "pricePerKg": 100 },
    { "Name": "Bannana", "color": "Yellow", "pricePerKg": 30 },
    { "Name": "Apple", "color": "red", "pricePerKg": 70 }
]

// Write your code here

// function to retrive the data from the Array
var retrieval = (f) => {
    fruits.forEach(x => {
        if (x.Name.toLowerCase() == f.toLowerCase()) {
            console.log(`color of the ${f} is ${x.color}`)
            console.log(`Price Per Kg  of ${f} is ${x.pricePerKg}`)
        }
    })
}

