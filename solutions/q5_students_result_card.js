// Write a program to display one result card of 100 students
// with their name and percentage
// Out of 100 students, 50 has subjects - Grammer and Accounts
// and other 50 has subjects -  Grammer and Physics

// Hint : You need to calculate percentage of 100 students having different set of subjects.
//        You can assume their scores on their respective subjects.


// Write your code here

// sample data considering only two students 
// considering total marks for each subject is 100 marks so total 200 marks for two subjects
var students = [
    {"id":1,"Name":"abc", "Grammer": 20, "Physics":30},
    {"id":2,"Name":"dlfg", "Grammer": 80, "Accounts":50}
]

// finding the percentage of all the students present in the students data 
var display = (data)=>{
    data.forEach(x => {
        if(Number.isInteger(x.Accounts)){
            console.log(`Student Roll No : ${x.id} -------->Student Name : ${x.Name} ------> Percentage is ${((x.Grammer + x.Accounts)*100)/200}%`)
        }
        else {
            console.log(`Student Roll No : ${x.id} -------->Student Name : ${x.Name} ------> Percentage is ${((x.Grammer + x.Physics)*100)/200 }%`)
        }
    })
}

display(students)